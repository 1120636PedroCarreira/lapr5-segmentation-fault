### Repositório para o projecto integrador - LAPRV ###

Este módulo deverá permitir a visualização em 3D dos quartos labirínticos. Deverá também apresentar, em sobreposição, um mini mapa de todo o labirinto. O módulo deverá suportar áudio para os efeitos especiais (ex., chuva, trovões) e eventual “música de fundo”.
Os mini jogo deverão ser apresentado num viewport sobreposto ao restante jogo usando a mesma janela do jogo principal (dentro do mesmo executável).
Poderão recorrer a modelos importados tais como, por exemplo, OBJ, 3DS ou MDL.

### Caracteristicas ###

Visual Studio 2013
C++ console App